require 'pirateify/version'
require 'engtagger'

class Pirateify
  ADJECTIVES = %w(so such very much many).freeze

  def initialize
    @tagger = EngTagger.new
  end

  def process(str)
    # Convert input to lowercase.
    str = str.downcase
    # "hello".gsub(/[aeiou]/, '*') 
    str.gsub('/[ng]/': 'n')
    # str.gsub('/ve/' : '\'')
    # str.gsub! 'i am' : 'i be'
    # str.gsub! 'you are' : 'you be'
    # str.gsub! 'they are' : 'they be'
    
  end

  private

  def adjective(i)
    ADJECTIVES[i % ADJECTIVES.size]
  end
end